@extends('vendor.installer.layouts.master')

@section('title', trans('messages.environment.title'))
@section('container')
    @if (session('message'))
        <p class="alert">{{ session('message') }}</p>
    @endif
    <form method="post" action="{{ route('LaravelInstaller::environmentSave') }}">
        @foreach($envConfig as $envKey => $envVal)
            <div @if(in_array($envkey,config('installer.env_ignore'))) class="hidden"@endif>
                <label for="{{$envKey}}">{{$envKey}}</label>
                <input type="text" class="text" name="{{$envKey}}" value="{{$envVal}}">
            </div>
        @endforeach
        {!! csrf_field() !!}
        <div class="buttons">
            <button class="button" type="submit">{{ trans('messages.environment.save') }}</button>
        </div>
    </form>
@stop
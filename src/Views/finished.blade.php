@extends('vendor.installer.layouts.master')

@section('title', trans('messages.final.title'))
@section('container')
    <p class="paragraph">{{ session('message')['message'] }}</p>

    <form method="post" action="{{route('LaravelInstaller::saveexit')}}">
            <label for="first_name">{{ trans('global.fld.first_name') }}</label>
            <input type="text" class="text" name="first_name">

            <label for="first_name">{{ trans('global.fld.last_name') }}</label>
            <input type="text" class="text" name="last_name">

            <label for="email">{{ trans('global.fld.email') }}</label>
            <input type="text" class="text" name="email">

            <label for="password">{{ trans('global.fld.password') }}</label>
            <input type="password" class="text" name="password">

        {!! csrf_field() !!}
        <div class="buttons buttons--right">
            <button class="button button--light" type="submit">{{ trans('messages.final.exit') }}</button>
        </div>
    </form>
@stop
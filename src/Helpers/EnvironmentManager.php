<?php

namespace vlindelaravel\custominstaller\Helpers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class EnvironmentManager
{
    /**
     * @var string
     */
    private $envPath;

    /**
     * @var string
     */
    private $envExamplePath;

    /**
     * Set the .env and .env.example paths.
     */
    public function __construct()
    {
        $this->envPath = base_path('.env');
        $this->envExamplePath = base_path('.env.example');
    }

    /**
     * Get the content of the .env file.
     *
     * @return string
     */
    public function getEnvContent()
    {
        if (!file_exists($this->envPath)) {
            if (file_exists($this->envExamplePath)) {
                copy($this->envExamplePath, $this->envPath);
            } else {
                touch($this->envPath);
            }
        }
        $lines = file($this->envPath);
//        dd($lines);
        foreach ($lines as $line) {
            $line = trim(str_replace('"', '', $line));
            $components = explode('=', $line);
            if ($components[0] != '') {
                $key = $components[0];
                $val = isset($components[1]) ? $components[1] : '';
                $result[$key] = $val;
            }
        }
        return $result;
    }

    /**
     * Save the edited content to the file.
     *
     * @param Request $input
     * @return string
     */
    public function saveFile(Request $input)
    {
        $inputs = $input->all();
        $output ='';
        if($inputs['APP_KEY'] == "")
            $inputs['APP_KEY'] = "base64:M1e6KI0w0xDCTGK0mrM59vaBYi550WSc64UuJ+3LtTk";

        foreach ($inputs as $key => $input) {
            if($key == "_token")
                continue;
            if (strpos($input, ' ') !== false)
                $output .= "$key=\"$input\"\r\n";
            else
                $output .= "$key=$input\r\n";
        }

        $message = trans('messages.environment.success');

        try {
            file_put_contents($this->envPath, $output);
        } catch (Exception $e) {
            $message = trans('messages.environment.errors');
        }
        Artisan::call('key:generate');
    }
}
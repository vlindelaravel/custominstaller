<?php

namespace vlindelaravel\custominstaller\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use vlindelaravel\custominstaller\Helpers\InstalledFileManager;

/**
 * Class FinalController
 * @package vlindelaravel\custominstaller\Controllers
 */
class FinalController extends Controller
{
    /**
     * Update installed file and display finished view.
     *
     * @return \Illuminate\View\View
     */
    public function finish()
    {

        return view('vendor.installer.finished');
    }

    /**
     * @param InstalledFileManager $fileManager
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveexit(InstalledFileManager $fileManager, Request $request)
    {

        $admin = Role::where('name', 'admin')->first();

        $input = $request->all();
        if(!($user = User::where('email',$input['email'])->first()))
            $user = new User();

        $user->first_name = $input['first_name'];
        $user->last_name = $input['last_name'];
        $user->password = bcrypt($input['password']);
        $user->email = $input['email'];
        $user->save();
        if(!$user->hasRole('admin'))
            $user->attachRole($admin);

        \Artisan::call('iseed',['tables' => 'users,role_user', '--force' => 'mytable']);
        $fileManager->update();
        return redirect(route('trans.home'));
    }
}
